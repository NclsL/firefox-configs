const Config = {
    name: "niclas",
    scale: 1,
    Links: [
        [
            "official",
            [
                ["aktia", "https://www.aktia.fi"],
                ["hotmail", "https://outlook.live.com/mail/0/inbox"]
            ]
        ],
        [
            "other",
            [
                ["iotech", "https://bbs.io-tech.fi/forums/hyvaet-tarjoukset.100/"],
                ["hs", "https://www.hs.fi"]
            ]
        ],
        [
            "media",
            [
                ["youtube", "https://www.youtube.com"],
                ["twitch", "https://www.twitch.tv"],
                ["hltv", "https://www.hltv.org/matches"],
				["nordnet", "https://www.nordnet.fi/markkinakatsaus"],
            ]
        ],
        [
            "lut",
            [
                ["email", "http://www.lut.fi/o365mail"],
                ["uni", "https://uni.lut.fi/"],
                ["moodle", "http://moodle.lut.fi/my/"],
                ["sisu", "https://sis-lut.funidata.fi/"]
            ]
        ]
    ]
}

const Main = (() => {
    const list = document.getElementById("list");
    const names = document.querySelectorAll("[data-Name]");
    const search = document.getElementById("search");
    const form = document.forms[0];

    const init = () => {
        list.innerHTML = Config.Links.map(([gName, Links]) => `
            <li>
                <h1 onclick="this.parentNode.classList.toggle('hideChildren')">${gName}</h1>
                <ul>
                    ${Links.map(([lName, url]) => `
                        <li>
                            <a href="${url}">${lName}</a>
                        </li>`
                    ).join("")}
                </ul>
            </li>` 
        ).join("")
        
        names.forEach(el => {
            el.innerText = Config.name;
        });

        document.addEventListener("keydown", e => e.key.length === 1 && search.focus());
        search.addEventListener("keydown", () => (window.event ? event.keyCode : e.which) == 13 && form.submit());
    };

    return {
        init,
    };
})();

Main.init()
